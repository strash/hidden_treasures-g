extends Control


signal btnplay_pressed
signal btnlvl_pressed


var logo_pos: float = 110.0
var logo_offset: float = 20.0
var angle: float = 0.0


# BUILTINS - - - - - - - - -


func _process(delta: float) -> void:
	$Logotype.rect_position.y = logo_pos + sin(angle) * logo_offset
	angle += delta * 1.5


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -


func _on_BtnPlay_pressed() -> void:
	emit_signal("btnplay_pressed")


func _on_BtnSelectLvl_pressed() -> void:
	emit_signal("btnlvl_pressed")
