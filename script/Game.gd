extends Control


signal btnback_pressed


enum WHEEL { INCREASE, DECREASE, IDLE }


var level: int

var title_pos: float = 70.0
var title_offset: float = 10.0
var title_angle: float = 0.0

var logo_pos: float = 480.0
var logo_offset: float = 10.0
var logo_angle: float = 0.0

var wheel_angle: float = 0.0
var wheel_speed: float = 0.0
var wheel_max_speed: float = 1500.0
var wheel_speed_step: float = 20.0

var wheel_state: int = WHEEL.IDLE


# BUILTINS - - - - - - - - -


func _ready() -> void:
	pass


func _process(delta: float) -> void:
	# title
	$Title.rect_position.y = title_pos + sin(title_angle) * title_offset
	title_angle += delta * 1.5

	# logotype
	$Logotype.rect_position.y = logo_pos + cos(logo_angle) * logo_offset
	logo_angle += delta * 1.5

	# wheel
	if wheel_state != WHEEL.IDLE:
		$Wheel.set_rotation(deg2rad(wheel_angle))
		wheel_angle += delta * wheel_speed
	if wheel_state == WHEEL.INCREASE:
		if wheel_speed > wheel_max_speed:
			wheel_speed = wheel_max_speed
		else:
			wheel_speed += wheel_speed_step
	elif wheel_state == WHEEL.DECREASE:
		if wheel_speed < 0:
			wheel_speed = 0
			wheel_state = WHEEL.IDLE
			var wrapped_angle: float = wrapf($Wheel.get_rotation(), 0.0, PI * 2)
			$Wheel.set_rotation(wrapped_angle)
			set_nearest_angle(wrapped_angle)
		else:
			wheel_speed -= wheel_speed_step


# METHODS - - - - - - - - -


func set_nearest_angle(angle: float) -> void:
	var angle_in_deg: int = round(rad2deg(angle)) as int
	var remainder: int = round(fmod(rad2deg(angle) + 30, 60.0)) as int
	var nearest_angle: int
	if remainder > 30:
		nearest_angle = angle_in_deg - (remainder - 30)
	else:
		nearest_angle = angle_in_deg + (30 - remainder)
	$Tween.interpolate_property($Wheel, "rect_rotation", angle_in_deg, nearest_angle, 0.2)
	wheel_angle = nearest_angle
	if not $Tween.is_active():
		$Tween.start()


func load_level() -> void:
	match level:
		1:
			$Bug.set_rotation(deg2rad(50))
			$Tween.interpolate_property($Man1, "modulate:a", 0.0, 1.0, 0.3)
		2:
			$Bug.set_rotation(deg2rad(-50))
			$Tween.interpolate_property($Man2, "modulate:a", 0.0, 1.0, 0.3)
	$Tween.interpolate_property($Wheel, "rect_scale", Vector2.ZERO, Vector2.ONE, 0.3)
	$Tween.interpolate_property($Wheel, "modulate:a", 0.0, 1.0, 0.3)
	var pointer_pos: float = get_viewport_rect().size.x / 2 + 132.5
	var pointer_pos_out: float = get_viewport_rect().size.x + $Pointer.rect_size.x
	$Tween.interpolate_property($Pointer, "rect_position:x", pointer_pos_out, pointer_pos, 0.3)
	var logotype_pos: float = (get_viewport_rect().size.x - $Logotype.rect_size.x) / 2
	var logotype_pos_out: float = 0 - $Logotype.rect_size.x
	$Tween.interpolate_property($Logotype, "rect_position:x", logotype_pos_out, logotype_pos, 0.3)
	$Tween.interpolate_property($Bug, "rect_position:y", 850.0, 548.0, 0.3)
	$Tween.interpolate_property($BtnSpin, "rect_position:y", 850.0, 666.0, 0.3)
	if not $Tween.is_active():
		$Tween.start()


func clear_level() -> void:
	match level:
		1:
			$Tween.interpolate_property($Man1, "modulate:a", 1.0, 0.0, 0.3)
		2:
			$Tween.interpolate_property($Man2, "modulate:a", 1.0, 0.0, 0.3)
		_:
			$Tween.interpolate_property($Man1, "modulate:a", 1.0, 0.0, 0.3)
			$Tween.interpolate_property($Man2, "modulate:a", 1.0, 0.0, 0.3)
	$Tween.interpolate_property($Wheel, "rect_scale", Vector2.ONE, Vector2.ZERO, 0.3)
	$Tween.interpolate_property($Wheel, "modulate:a", 1.0, 0.0, 0.3)
	var pointer_pos: float = get_viewport_rect().size.x / 2 + 132.5
	var pointer_pos_out: float = get_viewport_rect().size.x + $Pointer.rect_size.x
	$Tween.interpolate_property($Pointer, "rect_position:x", pointer_pos, pointer_pos_out, 0.3)
	var logotype_pos: float = (get_viewport_rect().size.x - $Logotype.rect_size.x) / 2
	var logotype_pos_out: float = 0 - $Logotype.rect_size.x
	$Tween.interpolate_property($Logotype, "rect_position:x", logotype_pos, logotype_pos_out, 0.3)
	$Tween.interpolate_property($Bug, "rect_position:y", 548.0, 850.0, 0.3)
	$Tween.interpolate_property($BtnSpin, "rect_position:y", 666.0, 850.0, 0.3)
	if not $Tween.is_active():
		$Tween.start()


# SIGNALS - - - - - - - - -


func _on_BtnSpin_pressed() -> void:
	if wheel_state == WHEEL.IDLE:
		wheel_state = WHEEL.INCREASE
		yield(get_tree().create_timer(rand_range(2.8, 4.0)), "timeout")
		wheel_state = WHEEL.DECREASE


func _on_BtnBack_pressed() -> void:
	if wheel_state == WHEEL.IDLE:
		emit_signal("btnback_pressed")
