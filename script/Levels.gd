extends Control


signal level_selected
signal btnback_pressed


var title_pos: float = 70.0
var title_offset: float = 10.0
var angle: float = 0.0


# BUILTINS - - - - - - - - -


func _process(delta: float) -> void:
	$Title.rect_position.y = title_pos + sin(angle) * title_offset
	angle += delta * 1.5


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -


func _on_BtnLvl1_pressed() -> void:
	emit_signal("level_selected", 1)


func _on_BtnLvl2_pressed() -> void:
	emit_signal("level_selected", 2)


func _on_BtnBack_pressed() -> void:
	emit_signal("btnback_pressed")
