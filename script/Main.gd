extends Node


enum VIEWS { MENU, LEVELS, GAME }


# BUILTINS - - - - - - - - -


func _ready() -> void:
	# MENU
	var _btnplay_pressed: int = $Menu.connect("btnplay_pressed", self, "_on_Menu_btnplay_pressed")
	var _btnlvl_pressed: int = $Menu.connect("btnlvl_pressed", self, "_on_Menu_btnlvl_presse")

	# LEVELS
	var _level_selected: int = $Levels.connect("level_selected", self, "_on_Levels_level_selected")
	var _levels_btnback_pressed: int = $Levels.connect("btnback_pressed", self, "_on_Levels_btnback_pressed")

	# GAME
	var _game_btnback_pressed: int = $Game.connect("btnback_pressed", self, "_on_Game_btnback_pressed")

	show_view(VIEWS.MENU)
	$Game.clear_level()


# METHODS - - - - - - - - -


func show_view(view: int) -> void:
	match view:
		VIEWS.MENU:
			$Menu.show()
			$Tween.interpolate_property($Menu, "modulate:a", 0.0, 1.0, 0.5)
			$Tween.interpolate_property($Levels, "modulate:a", 1.0, 0.0, 0.5)
			$Tween.interpolate_property($Game, "modulate:a", 1.0, 0.0, 0.5)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.5), "timeout")
			$Levels.hide()
			$Game.hide()
		VIEWS.LEVELS:
			$Levels.show()
			$Tween.interpolate_property($Menu, "modulate:a", 1.0, 0.0, 0.5)
			$Tween.interpolate_property($Levels, "modulate:a", 0.0, 1.0, 0.5)
			$Tween.interpolate_property($Game, "modulate:a", 1.0, 0.0, 0.5)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.5), "timeout")
			$Menu.hide()
			$Game.hide()
		VIEWS.GAME:
			$Game.show()
			$Tween.interpolate_property($Menu, "modulate:a", 1.0, 0.0, 0.5)
			$Tween.interpolate_property($Levels, "modulate:a", 1.0, 0.0, 0.5)
			$Tween.interpolate_property($Game, "modulate:a", 0.0, 1.0, 0.5)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.5), "timeout")
			$Menu.hide()
			$Levels.hide()


# SIGNALS - - - - - - - - -

# ----- MENU
func _on_Menu_btnplay_pressed() -> void:
	$Game.level = 1
	show_view(VIEWS.GAME)
	$Game.load_level()


func _on_Menu_btnlvl_presse() -> void:
	show_view(VIEWS.LEVELS)


# ----- LEVELS
func _on_Levels_level_selected(level: int) -> void:
	$Game.level = level
	show_view(VIEWS.GAME)
	yield(get_tree().create_timer(0.25), "timeout")
	$Game.load_level()


func _on_Levels_btnback_pressed() -> void:
	show_view(VIEWS.MENU)


# ----- GAME
func _on_Game_btnback_pressed() -> void:
	$Game.clear_level()
	yield(get_tree().create_timer(0.25), "timeout")
	show_view(VIEWS.MENU)
